package com.xsis.pos260.controllers;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.xsis.pos260.models.Variant;
import com.xsis.pos260.repositories.VariantRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiVariantController {
    @Autowired
    private VariantRepo variantRepo;

    @GetMapping(value = "/variant")
    public ResponseEntity<Page<Variant>> GetAllVariant(@RequestParam Integer page, @RequestParam Integer size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<Variant> variants = this.variantRepo.findAll(pageable);
            return new ResponseEntity<>(variants, HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping(value = "/variant")
    public ResponseEntity<Object> SaveVariant(@RequestBody Variant variant) {
        try {
            variant.setCreatedBy("Seno");
            variant.setCreatedOn(new Date());
            this.variantRepo.save(variant);
            return new ResponseEntity<>("success", HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/variant/search/{keyword}")
    public ResponseEntity<List<Variant>> SearchVariantByKeyword(@PathVariable("keyword") String keyword) {
        try {
            List<Variant> variants = this.variantRepo.SearchVariant(keyword);
            return new ResponseEntity<>(variants, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping(value = "/variant/{id}")
    public ResponseEntity<List<Variant>> GetVariantById(@PathVariable("id") Long id) {
        try {
            Optional<Variant> variant = this.variantRepo.findById(id);
            // kalo ada atau kosong
            if (variant.isPresent()) {
                // buat entity untuk return
                ResponseEntity rest = new ResponseEntity(variant, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping(value = "/variant/{id}")
    public ResponseEntity<Object> UpdateVariant(@RequestBody Variant variant, @PathVariable("id") Long id) {
        try {
            Optional<Variant> variantData = this.variantRepo.findById(id);

            if (variantData.isPresent()) {
                // set id biar nanti nge replace data dengan id ini
                variant.setId(id);
                variant.setModifieddBy("Seno");
                variant.setModifiedOn(new Date());
                this.variantRepo.save(variant);

                ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping(value = "/variant/{id}")
    public ResponseEntity<Object> DeleteVariant(@PathVariable("id") Long id) {
        try {
            Optional<Variant> variantData = this.variantRepo.findById(id);
            if (variantData.isPresent()) {
                // System.out.println(categoryData.get().getCategoryName());
                variantData.get().setId(id);
                variantData.get().setDelete(true);
                this.variantRepo.save(variantData.get());

                ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }

        } catch (Exception e) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/variant/category/{id}")
    public ResponseEntity<List<Variant>> GetVariantByCategoryId(@PathVariable("id") Long id) {
        try {
            List<Variant> variants = this.variantRepo.FindByCategoryId(id);
            return new ResponseEntity<>(variants, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
}
