package com.xsis.pos260.controllers;


import com.xsis.pos260.models.Category;
import com.xsis.pos260.models.Product;
import com.xsis.pos260.models.Variant;
import com.xsis.pos260.repositories.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*") // buat CORS,
@RequestMapping("/api")
public class ApiProductController {

    @Autowired
    private ProductRepo productRepo;

    @PostMapping(value = "/product")
    // ResponseEntity untuk return hasil success atau enggak nya
    public ResponseEntity<Object> SaveProduct(@RequestBody Product product) {
        try {
            product.setCreatedBy("Seno");
            product.setCreatedOn(new Date());
            this.productRepo.save(product);
            return new ResponseEntity<>("success", HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/product")
    public ResponseEntity<List<Product>> GetAllProduct() {
        try {
            List<Product> products = this.productRepo.findAll();
            return new ResponseEntity<>(products, HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping(value = "/product/search/{keyword}")
    public ResponseEntity<List<Product>> SearchProductByKeyword(@PathVariable("keyword") String keyword) {
        try {
            List<Product> products = this.productRepo.SearchProduct(keyword);
            return new ResponseEntity<>(products, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping(value = "/product/{id}")
    public ResponseEntity<List<Product>> GetProductById(@PathVariable("id") Long id) {
        try {
            Optional<Product> product = this.productRepo.findById(id);
            // kalo ada atau kosong
            if (product.isPresent()) {
                // buat entity untuk return
                ResponseEntity rest = new ResponseEntity(product, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping(value = "/product/{id}")
    public ResponseEntity<Object> UpdateVariant(@RequestBody Product product, @PathVariable("id") Long id) {
        try {
            Optional<Product> productData = this.productRepo.findById(id);

            if (productData.isPresent()) {
                // set id biar nanti nge replace data dengan id ini
                product.setId(id);
                product.setModifieddBy("Seno");
                product.setModifiedOn(new Date());
                this.productRepo.save(product);

                ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping(value = "/product/{id}")
    public ResponseEntity<Object> DeleteProduct(@PathVariable("id") Long id) {
        try {
            Optional<Product> productData = this.productRepo.findById(id);
            if (productData.isPresent()) {
                // System.out.println(categoryData.get().getCategoryName());
                productData.get().setId(id);
                productData.get().setDelete(true);
                this.productRepo.save(productData.get());

                ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }

        } catch (Exception e) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }
}
