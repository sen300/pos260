package com.xsis.pos260.controllers;

import com.xsis.pos260.models.Category;
import com.xsis.pos260.repositories.CategoryRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

//controller ini buat akses ke repo interface
@RestController
@CrossOrigin("*") // buat CORS,
@RequestMapping("/api")
public class ApiCategoryController {

    @Autowired
    private CategoryRepo categoryRepo;

    @PostMapping(value = "/category")
    // ResponseEntity untuk return hasil success atau enggak nya
    public ResponseEntity<Object> SaveCategory(@RequestBody Category category) {
        try {
            category.setCreatedBy("Seno");
            category.setCreatedOn(new Date());
            this.categoryRepo.save(category);
            return new ResponseEntity<>("success", HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/category")
    public ResponseEntity<List<Category>> GetAllCategory() {
        try {
            List<Category> categories = this.categoryRepo.findAll();
            // List<Category> categoriesFiltered = new ArrayList<>();
            //
            // for (Category category : categories) {
            // if (!category.isDelete()) {
            // categoriesFiltered.add(category);
            // }
            // }

            return new ResponseEntity<>(categories, HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping(value = "/category/search/{keyword}")
    public ResponseEntity<List<Category>> SearchCategoryByKeyword(@PathVariable("keyword") String keyword) {
        try {
            List<Category> categories = this.categoryRepo.SearchCategory(keyword);
            return new ResponseEntity<>(categories, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping(value = "/category/{id}")
    public ResponseEntity<List<Category>> GetCategoryById(@PathVariable("id") Long id) {
        try {
            Optional<Category> category = this.categoryRepo.findById(id);
            // kalo ada atau kosong
            if (category.isPresent()) {
                // buat entity untuk return
                ResponseEntity rest = new ResponseEntity(category, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping(value = "/category/{id}")
    public ResponseEntity<Object> UpdateCategory(@RequestBody Category category, @PathVariable("id") Long id) {
        try {
            Optional<Category> categoryData = this.categoryRepo.findById(id);

            if (categoryData.isPresent()) {
                // set id biar nanti nge replace data dengan id ini
                category.setId(id);
                category.setModifieddBy("Seno");
                category.setModifiedOn(new Date());
                this.categoryRepo.save(category);

                ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping(value = "/category/{id}")
    public ResponseEntity<Object> DeleteCategory(@PathVariable("id") Long id) {
        try {
            Optional<Category> categoryData = this.categoryRepo.findById(id);
            if (categoryData.isPresent()) {
                // System.out.println(categoryData.get().getCategoryName());
                categoryData.get().setId(id);
                categoryData.get().setDelete(true);
                this.categoryRepo.save(categoryData.get());

                ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }

        } catch (Exception e) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }
}
