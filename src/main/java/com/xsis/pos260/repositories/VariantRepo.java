package com.xsis.pos260.repositories;

import com.xsis.pos260.models.Variant;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface VariantRepo extends JpaRepository<Variant, Long> {
    @Query("FROM Variant WHERE CategoryId = ?1")
    List<Variant> FindByCategoryId(Long categoryId);

    @Query("FROM Variant WHERE lower(VariantName) LIKE lower(concat('%',?1,'%') ) ")
    List<Variant> SearchVariant(String keyword);

    @Override
    Page<Variant> findAll(Pageable pageable);
}
