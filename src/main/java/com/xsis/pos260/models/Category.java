package com.xsis.pos260.models;

import org.hibernate.annotations.Where;

import javax.persistence.*;

@Entity // ini adalah entity, anotasi adalah penanda
@Table(name = "category")
@Where(clause = "is_delete = false")
public class Category extends Common {
    @Id
    // @SequenceGenerator(
    // name = "category_sequence",
    // sequenceName = "category_sequence",
    // allocationSize = 1
    // )
    @GeneratedValue(strategy = GenerationType.IDENTITY
    // generator = "category_sequence"
    )
    @Column(name = "id", nullable = false)
    private Long Id;

    @Column(name = "category_code", nullable = false)
    private String categoryCode;

    @Column(name = "category_name", nullable = false)
    private String categoryName;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
